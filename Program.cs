﻿using System;
using System.Collections.Generic;
using System.IO;
using MySql.Data.MySqlClient;
using CsvHelper;
using System.Net;
using System.Globalization;


namespace attori
{
    class Program
    {
        static void Main(string[] args)
        {
            string cs = @"server=localhost;userid=pluto;password=alex;database=analisi_programmazione_film";
            using var con = new MySqlConnection(cs);
            con.Open();
            Console.WriteLine($"mysql version:{con.ServerVersion}");

            DirectoryInfo d = new DirectoryInfo(@"/var/www/html/actors/attori/"); //Assuming Test is your Folder            
            FileInfo[] Files = d.GetFiles("*.csv"); //Getting Text files
            string str = "";

            foreach (FileInfo file in Files)
            {
                long contatore = 0;
                int idEmittente = 0;
                str = str + ", " + file.Name;
                var controlloFile = "SELECT File_name FROM File_parsati WHERE File_name = @str";
                Console.WriteLine(file.Name);
                using var cmd1 = new MySqlCommand(controlloFile, con);
                cmd1.Parameters.AddWithValue("@str", file.Name);
                cmd1.Prepare();

                try
                {
                    using MySqlDataReader rdr = cmd1.ExecuteReader();
                    if (rdr.Read())
                    {
                        Console.WriteLine("Già Parsato: {0}", rdr.GetString(0));
                    }
                    else if (file.Name != "BancaDati Audiovisivo1.csv")
                    {
                        rdr.Close();
                        Console.WriteLine("file da parsare:" + file.Name);
                        var inserimento_file_parsato = "INSERT INTO File_parsati(File_name,Data_file) VALUES (@File_name,@Data_file)";
                        using var cmd2 = new MySqlCommand(inserimento_file_parsato, con);
                        cmd2.Parameters.AddWithValue("@File_name", file.Name);
                        cmd2.Parameters.AddWithValue("@Data_file", DateTime.Now.ToString());
                        cmd2.Prepare();
                        try
                        {
                            cmd2.ExecuteNonQuery();
                            var idFileParsato = cmd2.LastInsertedId;
                            Console.WriteLine("file aggiunto: " + idFileParsato);


                            if (file.Name != "BancaDati Audiovisivo1.csv")
                            {
                                TextReader reader1 = new StreamReader(file.Name);
                                var csvReader1 = new CsvReader(reader1, new System.Globalization.CultureInfo("it-it"));
                                var trasmissionifromcsv = csvReader1.GetRecords<Trasmissioni>();
                                foreach (Trasmissioni trasmissionefromcsv in trasmissionifromcsv)
                                {
                                    Console.WriteLine("vediamo se scorre");
                                    var controlloEmittente = "SELECT ID FROM Emittenti WHERE nomerete = @nome_emittente";
                                    using var cmd6 = new MySqlCommand(controlloEmittente, con);
                                    cmd6.Parameters.AddWithValue("@nome_emittente", trasmissionefromcsv.canale);
                                    cmd6.Prepare();
                                    try
                                    {
                                        using MySqlDataReader rdrEmittente = cmd6.ExecuteReader();
                                        if (rdrEmittente.Read())
                                        {
                                            idEmittente = rdrEmittente.GetInt32(0);
                                            //Console.WriteLine("Read: {0}", rdr.GetString(0));
                                        }
                                        else
                                        {
                                            rdrEmittente.Close();
                                            var inserisciEmittente = "INSERT INTO Emittenti (nomerete) VALUES (@nome_emittente)";
                                            using var cmd5 = new MySqlCommand(inserisciEmittente, con);
                                            cmd5.Parameters.AddWithValue("@nome_emittente", trasmissionefromcsv.canale);
                                            cmd5.Prepare();
                                            try
                                            {
                                                cmd5.ExecuteNonQuery();
                                                idEmittente = ((int)cmd5.LastInsertedId);
                                            }
                                            catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
                                        }
                                    }
                                    catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
                                    var sql = "INSERT INTO Trasmissioni (canale,data,orainizio,orafine,titoloopera,titolooperaoriginale,titolopuntata,numeropuntata,numerostagione,regia,id_file_provenienza) VALUES (@canale,@data,@orainizio,@orafine,@titoloopera,@titolooperaoriginale,@titolopuntata,@numeropuntata,@numerostagione,@regia,@id_file_provenienza)";
                                    using var cmd3 = new MySqlCommand(sql, con);
                                    cmd3.Parameters.AddWithValue("@canale", idEmittente);
                                    cmd3.Parameters.AddWithValue("@data", trasmissionefromcsv.Data);
                                    cmd3.Parameters.AddWithValue("@orainizio", trasmissionefromcsv.Ora_Inizio);
                                    cmd3.Parameters.AddWithValue("@orafine", trasmissionefromcsv.Ora_fine);
                                    cmd3.Parameters.AddWithValue("@titoloopera", trasmissionefromcsv.TITOLO_OPERA);
                                    cmd3.Parameters.AddWithValue("@titolooperaoriginale", trasmissionefromcsv.TITOLO_OPERA_ORIGINALE);
                                    cmd3.Parameters.AddWithValue("@titolopuntata", trasmissionefromcsv.TITOLO_PUNTATA);
                                    cmd3.Parameters.AddWithValue("@titolopuntataoriginale", trasmissionefromcsv.TITOLO_OPERA_ORIGINALE);
                                    cmd3.Parameters.AddWithValue("@numeropuntata", trasmissionefromcsv.NUMERO_PUNTATA);
                                    cmd3.Parameters.AddWithValue("@numerostagione", trasmissionefromcsv.NUMERO_STAGIONE);
                                    cmd3.Parameters.AddWithValue("@regia", trasmissionefromcsv.REGIA);
                                    cmd3.Parameters.AddWithValue("@id_file_provenienza", idFileParsato);
                                    cmd3.Prepare();
                                    try
                                    {
                                        cmd3.ExecuteNonQuery();
                                        contatore++;

                                        Console.WriteLine("record aggiunto");
                                    }
                                    catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
                                }
                            }
                            else
                            {
                                Console.WriteLine("passa");
                            }
                                Console.WriteLine("Ci passi l'olio?");
                                string updateFileParsato = "UPDATE File_parsati SET Num_righe = @numero_righe WHERE ID = @id_file_provenienza";
                                using var cmd4 = new MySqlCommand(updateFileParsato, con);
                                cmd4.Parameters.AddWithValue("@numero_righe", contatore);
                                cmd4.Parameters.AddWithValue("@id_file_provenienza", idFileParsato);
                                cmd4.Prepare();
                                try
                                {
                                    cmd4.ExecuteNonQuery();
                                }
                                catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
                        }
                        catch (Exception e)
                        { Console.WriteLine("errore" + e.ToString()); }
                    }
                    else{
                        Console.WriteLine("Niente BancaDati");
                    }

                }
                catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
            }

        }
    }


    // TextReader reader1 = new StreamReader("TOP CRIME_20200101_20201231.csv");
    // var csvReader1 = new CsvReader(reader1, new System.Globalization.CultureInfo("it-IT"));
    // var trasmissionifromcsv = csvReader1.GetRecords<Trasmissioni>();

    // foreach (Trasmissioni trasmissionefromcsv in trasmissionifromcsv)
    // {

    //     var sql = "INSERT INTO Trasmissioni(canale,data,orainizio,orafine,titoloopera,titolooperaoriginale,titolopuntata,numeropuntata,numerostagione,regia) VALUES (@nomerete,@data,@orainizio,@orafine,@titoloopera,@titolooperaoriginale,@titolopuntata,@numeropuntata,@numerostagione,@regia)";
    //     using var cmd = new MySqlCommand(sql, con);
    //     cmd.Parameters.AddWithValue("@nomerete",trasmissionefromcsv.Nome_Rete);
    //     cmd.Parameters.AddWithValue("@data", trasmissionefromcsv.Data);
    //     cmd.Parameters.AddWithValue("@orainizio", trasmissionefromcsv.Ora_Inizio);
    //     cmd.Parameters.AddWithValue("@orafine", trasmissionefromcsv.Ora_fine);
    //     cmd.Parameters.AddWithValue("@titoloopera",trasmissionefromcsv.TITOLO_OPERA );
    //     cmd.Parameters.AddWithValue("@titolooperaoriginale",trasmissionefromcsv.TITOLO_OPERA_ORIGINALE);
    //     cmd.Parameters.AddWithValue("@titolopuntata", trasmissionefromcsv.TITOLO_PUNTATA);
    //     cmd.Parameters.AddWithValue("@numeropuntata",trasmissionefromcsv.NUMERO_PUNTATA);
    //     cmd.Parameters.AddWithValue("@numerostagione",trasmissionefromcsv.NUMERO_STAGIONE);
    //     cmd.Parameters.AddWithValue("@regia",trasmissionefromcsv.REGIA);
    //     cmd.Prepare();
    //     try
    //     {
    //         cmd.ExecuteNonQuery();
    //         Console.WriteLine("La riga è stata inserita");
    //     }
    //     catch (Exception e)
    //     {
    //         Console.WriteLine("Errore " + e.ToString());
    //     }
    // }

}
class Trasmissioni
{
    public string canale { get; set; }
    public string Data { get; set; }

    public string Ora_Inizio { get; set; }
    public string Ora_fine { get; set; }
    public string TITOLO_OPERA { get; set; }

    public string TITOLO_OPERA_ORIGINALE { get; set; }
    public string TITOLO_PUNTATA { get; set; }
    public string TITOLO_PUNTATA_ORIGINALE { get; set; }
    public string NUMERO_PUNTATA { get; set; }
    public string NUMERO_STAGIONE { get; set; }
    public string REGIA { get; set; }


}
class Emittenti
{
    public string nome_rete { get; set; }
    public int id_gruppo { get; set; }
}
class BancaDati
{
    public string titoloitaliano { get; set; }
    public string titolooriginale { get; set; }

    public string numero_stagione { get; set; }
    public string episodio_puntata_titolo { get; set; }
    public string episodio_puntata_numero { get; set; }

    public string artista_rasi_primari_cognome { get; set; }
    public string artista_rasi_primari_nome { get; set; }
    public string artista_rasi_primari_pseudonimo { get; set; }
    public string artista_rasi_comprimari_cognome { get; set; }
    public string artista_rasi_comprimari_nome { get; set; }
    public string artista_rasi_comprimari_pseudonimo { get; set; }
    public string artista_rasi_doppiatoriprimari_cognome { get; set; }
    public string artista_rasi_doppiatoriprimari_nome { get; set; }
    public string artista_rasi_doppiatoriprimari_pseudonimo { get; set; }
    public string artista_rasi_doppiatoricomprimari_cognome { get; set; }
    public string artista_rasi_doppiatoricomprimari_nome { get; set; }
    public string artista_rasi_doppiatoricomprimari_pseudonimo { get; set; }
    public string regia { get; set; }
    public string anno_di_pubblicazione { get; set; }
    public string rappresentazione { get; set; }
    public string distribuzione { get; set; }
    public string anno_di_produzione { get; set; }
    public string tipologia_dell_opera { get; set; }
    public string tipo_di_trasmissione { get; set; }
    public string paese_di_produzione { get; set; }

}